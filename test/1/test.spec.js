'use strict';

describe('npmConfig', function () {

	it("should have config loaded", function (done) {

		var $module = require(__dirname + '/module');

		expect($module.drinks).toEqual({
			alcohol: 'yes',
			caffeine: 'yes'
		});

		done();

	});

	it('should have the same config loaded for module 2', function (done) {

		var $module = require(__dirname + '/module');
		var $module2 = require(__dirname + '/module');

		expect($module).toEqual($module2);

		done();

	});

	it('should update both config files when one changes', function (done) {

		var $module = require(__dirname + '/module');
		var $module2 = require(__dirname + '/module');

		$module.biggerBooger = true;

		expect($module).toEqual($module2);

		done();

	});

});
