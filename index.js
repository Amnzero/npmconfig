'use strict';

var $path = require('path');
var $fs = require('fs');

var initialized = false;



module.exports = npmConfig;
module.exports.__refresh = refresh;

// search for npmConfig.json in current path or higher
function npmConfig (path) {


	if (!initialized) {

		initialized = true;

		path = $path.resolve(path || '.');

		var pathParts = path.split($path.sep);

		while (pathParts.length) {

			if ($fs.existsSync(path + '/npmConfig.json')) {

				_parseConfigFile(path);
				break;

			}

			// remove last folder
			pathParts.pop();
			// join on system separator
			path = pathParts.join($path.sep);

		}

	}

	return npmConfig;

}

function refresh (path) {

	initialized = false;
	return npmConfig(path);

}

function _parseConfigFile (path) {

	var config = require(path + '/npmConfig.json');

	Object.keys(config).forEach(function (value) {

		npmConfig[value] = require($path.resolve(path, config[value]));

	});

}