# npmConfig -- portable config object

## Why?

No need for globals, and simple initialisation of the config object no matter where in the app you require it. 

## Usage

The config returns a function that will be extended when adding config. It is a single instance that is shared for the entire application, so changing values in module A will mean changes have occured in module B.

The function takes an optional `path` parameter, which will determine where it will start searching for the `npmConfig.json` file. It will traverse up the tree until it has been found or will return as-is otherwise.<br>
Usually it is a good idea to pass in `__dirname` so it will search upwards from the current module path.
When left out, it will use `'.'`. 

### Manual definition

```
// file.js
var config = require('npmConfig');
config.something = { your: 'config' };

// file2.js
var config = require('npmConfig');
console.log(config.something); // { your: 'config' }
```

### Generated definition
Place an `npmConfig.json` file in your project or app root. It will `require()` the values of the JSON and assign them to the config object under the matching key.

```
// npmConfig.json
{
	"paths": "config/paths",
	"settings": "config/settings"
}

// config/paths.js
module.exports = { root: './' };

// config/settings.js
module.exports = { switch: 'on' };

// module.js
var config = require('npmConfig')(__dirname);
console.log(config.something);
/*
{ 
	paths:  { root: './' }, 
	settings: { switch: 'on' }
}
*/
```